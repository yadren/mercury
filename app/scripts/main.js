$(document).ready(function () {

	// Меню открытие
	$('.navbar-toggle').click(function () {
		if ($(this).is('.navbar-toggle-js')) {
			$('.navbar-wrapper').fadeOut();
		} else {
			$('.navbar-wrapper').fadeIn();
		}
		$(this).toggleClass('navbar-toggle-js active');
		//$('body').toggleClass('no_scroll');
	});

	// Слайдер на главной
	$('.slider_promo').slick({
		dots: true,
		speed: 900,
		autoplay: true,
		autoplaySpeed: 2000,
	});

	$('.slick-dots li').each(function () {
		$(this).find('button').text('0' + ($(this).index() + 1));
	});

	// Показ фильтров и оверлея для скрытия первой строки товаров
	var sort_btn = $('.sort_item__btn');
	//var sortOverlay = $('.sort-overlay');
	
	sort_btn.click(function () {
		
		sort_btn.not(this).next().slideUp().removeClass('active');
		sort_btn.not(this).removeClass('active');
		//sortOverlay.css('height', $('.product_grid').filter(':first').outerHeight(true))
		$(this).toggleClass('active').next().slideToggle().toggleClass('active');
		
		// if($('.sort_item__btn').hasClass('active')){
		// 	sortOverlay.addClass('active');
		// 	} else{
		// 		sortOverlay.removeClass('active');
		// 		sortOverlay.css('height', 0)
		// 	}
	});


	// sortOverlay.click(function () {
	// 	this.style;
	// 	mobileFiltersClose.toggleClass('active');
	// });

	var filter = $('.b_sort_by_wrapper');
	var mobileFiltersOpen = $('.sort_btn_toogle_mob');
	var mobileFiltersClose = $('.sort_item__close');

	mobileFiltersOpen.click(function () {
		filter.slideToggle();
		mobileFiltersClose.toggleClass('active');
	});

	mobileFiltersClose.click(function () {
		$(this).removeClass('active');
		filter.slideToggle();
	});

	// Этапы работы на главной
	$('.work_step').mouseover(function () {
		$(this).find('.index_step').addClass('active');
	}).mouseout(function () {
		$(this).find('.index_step').removeClass('active');
	});


	$('.navbar-title').click(function () {
		$(this).toggleClass('active')
		$('.subnavbar-nav').slideToggle().toggleClass('active');
		return false;
	});

	// Каталог

	$(' .title_filter, .subnavbar-nav, .category_filter.dropdown, .title_tabs_caregory').click(function () {
		$(this).toggleClass('active').next().slideToggle().toggleClass('active');
		return false;
	});


	if ($(this).width() < 1200) {
		/* $('.sort_btn_toogle_mob').click(function () {
			$(this).toggleClass('active').next().slideToggle().toggleClass('active');
			$('.title_catalog_filters').closest('.b_catalog_filters').find('.wrap_filters_slide').slideUp();
		}); */
		$('.factory_subfilter > .filter.subfilter').appendTo( $('.wrap_filters_first-level') );

		$('.title_catalog_filters').click(function () {
			$(this).closest('.b_catalog_filters').find('.wrap_filters_slide').slideToggle();
			//$('.sort_btn_toogle_mob').next().slideUp();
		});

		$('.b_catalog_filters, .title_catalog_filters').click(function () {
			$(this).toggleClass('active');
		});
	}	
	
	if(($('.b_catalog_filters').hasClass('filters_with-submeny'))  && $(window).width() > 1200 ) {
		$('.title_catalog_filters').click(function () {
			$(this).closest('.b_catalog_filters').find('.wrap_filters_slide').slideToggle();
		});
		
		$('.b_catalog_filters, .title_catalog_filters').click(function () {
			$(this).toggleClass('active');
		});		
	} 
	
  if ($('.b_catalog_filters').length > 0 && $(window).width() > 1200) {
		catalog_filters();
	}

	if (detectIE11()) {
		$('.order_callback_link, .call_link, .title_filter, .category_filter, .title_catalog_filters, .title_tabs_caregory, .navbar-nav>li>a').css({ 'background': 'none' });
	}

	var isSafari = window.safari !== undefined;
	if (isSafari) {
		$('.b_main_screen .slick-next, .b_main_screen .slick-prev').css('border', 'none');
	}

	$('select').styler();

	// Слайдер в продутке
	$('.slider_product').slick({
		dots: true,
		fade: true,
		arrows: false,
		/* customPaging: function (slider, i) {
				return '<button class="tab">' + $('.slider_product_thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
		} */
	});

	$('[data-fancybox]').fancybox({
		selector: '[data-fancybox="images"]',
		loop: true
	});

	$('.wrap_price_product').each(function () {
		var count = $(this).find('.label_product').length
		if (count > 1) {
			$(this).find('.product_price').addClass('bottom_product_price');
		}
	});

	// слайдер вы недавно смотрели
	$('.latest-view-slider').slick({
		slidesToShow: 7,
		slidesToScroll: 6,
		dots: true,
		//fade: true,
		arrows: false,
		variableWidth: true,
		responsive: [
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 6,
					slidesToScroll: 5,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 4,
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				}
			}
		]
		// autoplay: true,
		// autoplaySpeed: 2000,
	});

	// количество слайдев и текущий слайд 
	var $status = $('.bottom-slider__count');
	var $slickElement = $('.bottom-slider__list');
	$slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		$status.text(i + ' / ' + slick.slideCount);
	});

	// Нижний слайдер
	$('.bottom-slider__list').slick({
		dots: false,
		arrows: true,
		variableWidth: true,
		infinite: true,
		variableWidth: false,
		prevArrow: $('.slick-custom-prev'),
		nextArrow: $('.slick-custom-next')
	});

	// валидация
	function validate() {
		var errors = 0;
		$('.modal_form .form_input:visible').each(function () {
			if (!$(this).val() || $(this).val() >= 3) {
				errors++;
				$(this).addClass('error_inp').val('Не заполнено  поле ' + $(this).attr('placeholder'));
			} else if ($(this).val()) {
				$(this).removeClass('error_inp');
			}
		});

		if (errors > 0) {
			return 0;
		} else {
			return 1;
		}
	}

	// форма отправки 
	$('.form_input').focus(function () {
		if ($(this).is('.error_inp')) {
			$(this).val('');
		}
		$(this).removeClass('error_inp');
	});

	//форма отправки
	$('.btn_form').click(function () {
		if (!validate()) {
			return false;
		} else {
			//here write AJAX query
			$('.sucess-msg').addClass('active');
			setTimeout(function () {
				$('.sucess-msg').removeClass('active');
			}, 3000)
			return false;
		}
	});

	$('#phone').click(function () {
		$(this).val('+38 ');
		$(this).mask('+38 (000) 000-0000');
	});

	//сладйер диапазана >>>>

	$('#range-slider').slider({
		min: 0,
		max: 30000,
		values: [0, 30000],
		range: true
	});

	$('#range-slider').slider({
		min: 0,
		max: 30000,
		values: [0, 30000],
		range: true,
		stop: function (event, ui) {
			$('input#minCost').val($('#range-slider').slider('values', 0));
			$('input#maxCost').val($('#range-slider').slider('values', 1));
		},
		slide: function (event, ui) {
			$('input#minCost').val($('#range-slider').slider('values', 0));
			$('input#maxCost').val($('#range-slider').slider('values', 1));
		}
	});

	$('input#minCost').change(function () {
		var value1 = $('input#minCost').val();
		var value2 = $('input#maxCost').val();

		if (parseInt(value1) > parseInt(value2)) {
			value1 = value2;
			$('input#minCost').val(value1);
		}
		$('#range-slider').slider('values', 0, value1);
	});

	$('input#maxCost').change(function () {
		var value1 = $('input#minCost').val();
		var value2 = $('input#maxCost').val();

		if (value2 > 30000) { value2 = 30000; $('input#maxCost').val(30000) }

		if (parseInt(value1) > parseInt(value2)) {
			value2 = value1;
			$('input#maxCost').val(value2);
		}
		$('#range-slider').slider('values', 1, value2);
	});

	// Табы
	$('ul.tabs__caption').on('click', 'li:not(.active)', function () {
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});
});

$(window).on('load resize scroll', function () {
	if ($(this).width() < 1700) {
		$('.container').removeClass('col-lg-offset-3');
	} else {
		$('.container').addClass('col-lg-offset-3');
	}

	if ($(this).width() < 1700) {
		$('.b_catalog .col-lg-8').addClass('col-lg-9');
		$('.b_catalog .col-lg-9').removeClass('col-lg-8');
		$('.b_partners .col-lg-9').addClass('col-lg-8');
		$('.b_partners .col-lg-9').removeClass('col-lg-9');
	} else {
		$('.b_catalog .col-lg-8').removeClass('col-lg-9');
		$('.b_catalog .col-lg-9').addClass('col-lg-8');
		$('.b_partners .col-lg-9').addClass('col-lg-8');
		$('.B_partners .col-lg-9').removeClass('col-lg-9');
	}

	if ($(this).width() < 1600) {
		$('.content_main').addClass('col-sm-12');
		$('.content_main').removeClass('col-sm-offset-1');
	} else {
		$('.content_main').removeClass('col-sm-12');
		$('.content_main').addClass('col-sm-offset-1');
	}

	if ($(this).width() < 1500) {
		$('.product_grid').addClass('col-sm-4');
		$('.product_grid').removeClass('col-sm-3');
	} else {
		$('.product_grid').removeClass('col-sm-4');
		$('.product_grid').addClass('col-sm-3');
	}

	if ($(this).width() > 1200 && !($('.b_catalog_filters').hasClass('filters_with-submeny'))) {
		$('.wrap_filters_slide').show();
	}

	if ($(this).width() > 1186 && !(($('.b_catalog_filters').hasClass('filters_with-submeny')))) {
		$('.title_catalog_filters').addClass('active-default');
	}

	if ($(this).width() < 1186) {
		$('.title_catalog_filters').removeClass('active-default');
	}

	$('.loader').fadeOut();


});


function detectIE() {
	var ua = window.navigator.userAgent;
	var ie = ua.search(/(MSIE|Trident|Edge)/);

	return ie > -1;
}
function detectIE11() {
	var ua = window.navigator.userAgent;
	var ie = ua.search(/(MSIE|Trident)/);

	return ie > -1;
}

// каталог sticky
var catalog_filters = function () {
	var a = document.querySelector('.b_catalog_filters'), b = null, P = 120;
	window.addEventListener('scroll', Ascroll, false);
	document.body.addEventListener('scroll', Ascroll, false);
	function Ascroll() {
		if (b == null) {
			var Sa = getComputedStyle(a, ''), s = '';
			for (var i = 0; i < Sa.length; i++) {
				if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
					s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
				}
			}
			b = document.createElement('div');
			b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
			a.insertBefore(b, a.firstChild);
			var l = a.childNodes.length;
			for (var i = 1; i < l; i++) {
				b.appendChild(a.childNodes[1]);
			}
			a.style.height = b.getBoundingClientRect().height + 'px';
			a.style.padding = '0';
			a.style.border = '0';
		}
		var Ra = a.getBoundingClientRect(),
			R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.b_catalog').getBoundingClientRect().bottom + 115);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
		if ((Ra.top - P) <= 0) {
			$(a).addClass('fix_menu');
			if ((Ra.top - P) <= R) {
				b.className = 'stop';
				b.style.top = - R + 'px';

			} else {
				b.className = 'sticky';
				b.style.top = P + 'px';
			}
		} else {
			$(a).removeClass('fix_menu');
			b.className = '';
			b.style.top = '';
		}
		window.addEventListener('resize', function () {
			a.children[0].style.width = getComputedStyle(a, '').width
		}, false);
	}
}
